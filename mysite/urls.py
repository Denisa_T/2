from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^polls/', include('polls.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/register/$', 'accounts.views.register', name='register'),
    url(r'^accounts/register/complete/$', 'accounts.views.registration_complete', name='registration_complete'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^accounts/profile/$', 'accounts.views.profile', name='profile'),
]